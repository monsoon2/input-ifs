#! /bin/sh

set -o xtrace -o errexit -o nounset

infile=${1:-ifs_oper_T1279_2021071200.grb}
outfile=${2:-ifs2icon.nc}

# Define output grid
gridid=0015
gridname=R02B09
icon_grids=/pool/data/ICON/grids/public/mpim/

# Define variable name mapping
cat > ifs_select.expr << EOF
T=t;
U=u;
V=v;
W=w;
QV=q;
QC=clwc;
QI=ciwc;
QR=crwc;
QS=cswc;
GEOP_ML=z;
LNPS=lnsp;
SKT=skt;
STL1=stl1;
STL2=stl2;
STL3=stl3;
STL4=stl4;
SMIL1=swvl1;
SMIL2=swvl2;
SMIL3=swvl3;
SMIL4=swvl4;
W_SNOW=sd;
EOF

# Pre-calculate interpolation weights if not present
griddes=$(cdo griddes $infile | grep gridtype | cut -b 13-)
weights=weights/weights_${griddes}_${gridname}.nc
grid=$icon_grids/$gridid/icon_grid_${gridid}_${gridname}_G.nc

if [[ ! -f $weights ]]; then
  cdo -P 8 -gendis,$grid $infile $weights
fi


# Extract, rename, and interpolate input variables
cdo \
  -P 8 \
  -O \
  -f nc4 \
  -remap,$grid,$weights \
  -exprf,ifs_select.expr \
  $infile \
  $outfile

ncrename -d cell,ncells $outfile
