#! /bin/sh

set -o xtrace -o errexit -o nounset

infile=${1:-sst-sic-runmean_20210604-20210811_0015_R02B09_G.nc}
sstfile=${2:-bc_sst.nc}
sicfile=${2:-bc_sic.nc}

# Define output grid
gridid=0043
gridname=R02B04
icon_grids=/pool/data/ICON/grids/public/mpim/

# Pre-calculate interpolation weights if not present
griddes=$(cdo griddes $infile | grep gridtype | cut -b 13-)
weights=weights_${griddes}_${gridname}.nc
grid=$icon_grids/$gridid/icon_grid_${gridid}_${gridname}_G.nc

if [[ ! -f $weights ]]; then
  cdo -P 8 -gendis,$grid $infile $weights
fi

# Extract, rename, and interpolate input variables
cdo -P 8 -O -f nc4 -remap,$grid,$weights -monmean -select,name=SST $infile $sstfile
cdo -P 8 -O -f nc4 -remap,$grid,$weights -monmean -mulc,100 -select,name=SIC $infile $sicfile
