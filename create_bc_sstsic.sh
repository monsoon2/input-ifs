#! /bin/sh

set -o xtrace -o errexit -o nounset

infile=${1:-202107-202108_sst-sic.grb}
sstsicfile=${2:-sst-sic-runmean_oper_202107-202108.nc}

# Define output grid
gridid=0043
gridname=R02B04
icon_grids=/pool/data/ICON/grids/public/mpim/

# Pre-calculate interpolation weights if not present
griddes=$(cdo griddes $infile | grep gridtype | cut -b 13-)
weights=weights/weights_${griddes}_${gridname}.nc
grid=$icon_grids/$gridid/icon_grid_${gridid}_${gridname}_G.nc

if [[ ! -f $weights ]]; then
  cdo -P 8 -gendis,$grid $infile $weights
fi

# Extract, rename, and interpolate input variables
cdo -P 8 -O -t ecmwf -f nc4 -remap,$grid,$weights -expr,"SST=SSTK;SIC=CI" $infile $sstsicfile
